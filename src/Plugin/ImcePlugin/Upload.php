<?php

namespace Drupal\imce_flysystem\Plugin\ImcePlugin;

use Drupal\imce\Plugin\ImcePlugin\Upload as ImcePluginUpload;

/**
 * Defines Imce Upload plugin.
 *
 * @ImcePlugin(
 *   id = "flysystem upload",
 *   label = "Flysystem Upload",
 *   scheme_provider = "flysystem",
 *   weight = -90,
 *   operations = {
 *     "upload" = "opUpload"
 *   }
 * )
 */
class Upload extends ImcePluginUpload {

}
