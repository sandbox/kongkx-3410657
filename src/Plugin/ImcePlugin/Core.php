<?php

namespace Drupal\imce_flysystem\Plugin\ImcePlugin;

use Drupal\Core\Session\AccountProxyInterface;

use Drupal\imce\ImceFM;
use Drupal\imce\Plugin\ImcePlugin\Core as ImcePluginCore;
use Drupal\imce_flysystem\FlysystemTrait;

/**
 * Defines Imce Core plugin.
 *
 * @ImcePlugin(
 *   id = "flysystem_core",
 *   label = "Flysystem Core",
 *   scheme_provider = "flysystem",
 *   weight = -100,
 *   operations = {
 *     "browse" = "opBrowse",
 *     "uuid" = "opUuid"
 *   }
 * )
 */
class Core extends ImcePluginCore {

  use FlysystemTrait;

  /**
   * Operation handler: browse.
   */
  public function opBrowse(ImceFM $fm) {
    if ($folder = $fm->activeFolder) {
      $uri = $folder->getUri();
      $content = ['props' => $this->getFolderProperties($uri)];
      $inner = $this->scanDir($uri, [
        'browse_files' => $folder->getPermission('browse_files'),
        'browse_subfolders' => $folder->getPermission('browse_subfolders'),
      ]);
      $content['files'] = $inner['files'];
      $content['subfolders'] = $inner['subfolders'];
      $fm->addResponse('content', $content);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processUserConf(array &$conf, AccountProxyInterface $user) {
    $schemes = \Drupal::service('flysystem_factory')->getSchemes();
    if (in_array($conf['scheme'], $schemes)) {
      $conf['scheme_provider'] = 'flysystem';
    }
  }

}
