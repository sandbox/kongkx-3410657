<?php

namespace Drupal\imce_flysystem\Plugin\ImcePlugin;

use Drupal\imce\Plugin\ImcePlugin\Newfolder as ImcePluginNewfolder;

/**
 * Defines Imce New Folder plugin.
 *
 * @ImcePlugin(
 *   id = "newfolder",
 *   label = "New Folder",
 *   weight = -15,
 *   scheme_provider = "flysystem",
 *   operations = {
 *     "newfolder" = "opNewfolder"
 *   }
 * )
 */
class Newfolder extends ImcePluginNewfolder {

}
