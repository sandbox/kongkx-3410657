<?php

namespace Drupal\imce_flysystem\Plugin\ImcePlugin;

use Drupal\imce\Plugin\ImcePlugin\Delete as ImcePluginDelete;

/**
 * Defines Imce Delete plugin.
 *
 * @ImcePlugin(
 *   id = "flysystem_delete",
 *   label = "Flysystem Delete",
 *   scheme_provider = "flysystem",
 *   weight = -80,
 *   operations = {
 *     "delete" = "opDelete"
 *   }
 * )
 */
class Delete extends ImcePluginDelete {

}
