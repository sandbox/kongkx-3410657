<?php

namespace Drupal\imce_flysystem\Plugin\ImcePlugin;

use Drupal\imce\Plugin\ImcePlugin\Resize as ImcePluginResize;

/**
 * Defines Imce Resize plugin.
 *
 * @ImcePlugin(
 *   id = "flysystem resize",
 *   label = "Flysystem Resize",
 *   scheme_provider = "flysystem",
 *   weight = -20,
 *   operations = {
 *     "resize" = "opResize"
 *   }
 * )
 */
class Resize extends ImcePluginResize {

}
