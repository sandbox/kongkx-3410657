<?php

namespace Drupal\imce_flysystem;

use Drupal\imce\ImceFM;

/**
 *
 */
trait FlysystemTrait {
  /**
   * Filesystem.
   *
   * @var \League\Flysystem\Filesystem
   */
  protected $filesystem;

  /**
   * Parse scheme and path from uri.
   */
  protected function parseUri(string $uri) {
    if (preg_match('/^([a-z\-]+):\/\/(.*)?$/', $uri, $matches)) {
      $params = [
        'scheme' => $matches[1],
        'path' => $matches[2] ?? '/',
      ];
      if (empty($params['path'])) {
        $params['path'] = '/';
      }
      return $params;
    }
    return FALSE;
  }

  /**
   * Get Filesystem.
   */
  protected function getFilesystem(string $uri) {
    if (is_null($this->filesystem)) {
      $params = $this->parseUri($uri);
      if ($params) {
        $this->filesystem = \Drupal::service('flysystem_factory')->getFilesystem($params['scheme']);
      }
    }
    return $this->filesystem;
  }

  /**
   * Returns the contents of a directory.
   */
  protected function scanDir($dir_uri, array $options = []) {
    $content = ['files' => [], 'subfolders' => []];
    $browse_files = $options['browse_files'] ?? TRUE;
    $browse_subfolders = $options['browse_subfolders'] ?? TRUE;
    if (!$browse_files && !$browse_subfolders) {
      return $content;
    }
    // Prepare filters.
    $name_filter = empty($options['name_filter']) ? FALSE : $options['name_filter'];
    $callback = empty($options['filter_callback']) ? FALSE : $options['filter_callback'];
    $uriprefix = substr($dir_uri, -1) === '/' ? $dir_uri : $dir_uri . '/';

    $params = $this->parseUri($dir_uri);
    $filesystem = $this->getFilesystem($dir_uri);
    $items = $filesystem->listContents($params['path']);
    foreach ($items as $item) {
      $filename = $item['basename'];
      // Exclude special names.
      if ($filename === '.' || $filename === '..') {
        continue;
      }
      // Check filter regexp.
      if ($name_filter && preg_match($name_filter, $filename)) {
        continue;
      }
      // Check browse permissions.
      $fileuri = $uriprefix . $filename;
      $is_dir = $item['type'] == 'dir';
      if ($is_dir ? !$browse_subfolders : !$browse_files) {
        continue;
      }
      // Execute callback.
      if ($callback) {
        $result = $callback($filename, $is_dir, $fileuri, $options);
        if ($result === 'continue') {
          continue;
        }
        elseif ($result === 'break') {
          break;
        }
      }

      if ($is_dir) {
        $content['subfolders'][$filename] = $this->mapFolderProperties($item);
      }
      else {
        $content['files'][$filename] = $this->mapFileProperties($item, $fileuri);
      }
    }

    return $content;
  }

  /**
   * Returns js properties of a folder.
   */
  protected function getFolderProperties(string $uri) {
    $params = $this->parseUri($uri);
    $filesystem = $this->getFilesystem($uri);
    if ($params['path'] == '/') {
      return ['date' => 0];
    }
    $meta = $filesystem->getMetadata($params['path']);
    return $this->mapFolderProperties($meta);

  }

  /**
   * Returns js properties of a file.
   */
  protected function getFileProperties(string $uri, ImceFM $fm) {
    $params = $this->parseUri($uri);
    $filesystem = $this->getFilesystem($uri);
    $normalizedPath = \Normalizer::normalize($params['path'], \Normalizer::FORM_C);
    $meta = $filesystem->getMetadata(
      $normalizedPath
    );
    return $this->mapFileProperties($meta, $uri);

  }

  /**
   * Map folder properties from metadata.
   */
  protected function mapFolderProperties($meta) {
    return [
      'date' => $meta['timestamp'],
    ];
  }

  /**
   * Map file properties from metadata.
   */
  protected function mapFileProperties($meta, string $uri) {
    $properties = [
      'date' => $meta['timestamp'],
      'size' => $meta['size'],
      'url' => \Drupal::service('file_url_generator')->generateAbsoluteString($uri),
    ];

    // $regexp = $fm->conf['image_extensions_regexp'] ?? $fm->imageExtensionsRegexp();
    // @todo handle image size and thumbnail.
    return $properties;
  }

}
